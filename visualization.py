from _dataloader import generate_2dim, plt, np, torch
from _constraint_net import MLP_Constraint
from _iterative_proj import Projection

MODEL_DIR = "models/"
NAME = "rigid_4"
NUM_PARTICLES = 4
DIMENSION = 2
NUM_ITER = 5
C_LAYERS = [64, 32, 1]

def load_model():
    model_path = MODEL_DIR + NAME + "/" + "best_model.pt"

    func_net = MLP_Constraint(num_particles=NUM_PARTICLES,
                           dimension=DIMENSION, num_features=C_LAYERS)
    func_net.load_state_dict(torch.load(model_path))
    proj_model = Projection(num_particles=NUM_PARTICLES,
                            dimension=DIMENSION, constrains=func_net, num_iter=NUM_ITER).cpu()
    return proj_model

def test_projection():
    model = load_model()
    d1, l1 = generate_2dim(1)
    d2, l2 = generate_2dim(1)
    d3, l3 = generate_2dim(1)
    d4, l4 = generate_2dim(1)
    d = torch.cat((d1,d2,d3,d4), 1)
    l = torch.cat((l1,l2,l3,l4), 1)
    print(d.size(), l.size())
    pred = model(d)
    print(pred)
    pred2 = model(pred)
    pred = pred.detach().numpy()
    d = d.detach().numpy()
    plt.scatter(d[:,:,0], d[:,:,1], c = 'y')
    plt.scatter(l[:,:,0], l[:,:,1], c = 'g')
    plt.scatter(pred[:,:,0], pred[:,:,1], c = 'b')
    plt.show()
    
def show_implicit_function():
    cons = model.constrains
    x_ = np.arange(-1.5, 1.5, 0.01)
    y_ = np.arange(-1.5, 1.5, 0.01)
    x_, y_ = np.meshgrid(x_, y_)
    x = torch.Tensor(x_).view(x_.size, 1)
    y = torch.Tensor(y_).view(y_.size, 1)
    data = torch.cat((x, y), 1).view(x.size()[0], 4, 2)
    c = cons(data)
    c = c.view(x_.shape[0], x_.shape[1]).detach().numpy()

    fig = plt.figure(figsize=(6,6))
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(x_, y_, c, cmap = 'rainbow')
    plt.show()    

def print_params():
    model = load_model()
    for name, param in model.named_parameters():
        if param.requires_grad:
            print(name, param.data)

if __name__ == '__main__':
    # test_projection()
    # show_implicit_function()
    print_params()