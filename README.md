# Learning sparse physical constraints with neural projection

Using base code from Shuqi (https://github.com/y-sq/neural_proj), modified `_constraint_net.py` to use a sparse constraint. Currently only using code for the rigid_4 simulation.

## Code

Run `training_1_rigid_4.py` to produce model. Run `simulation_1_rigid_4.py` to create gif.

## Results

See results folder for gifs.

### with 6 lengths, 4 angles hard-coded:

validation loss = about 2e-4

learned weights:

```
constrains.net.fc1.weight tensor([[ 0.0600,  0.0756,  1.1371,  0.0623,  1.3211,  0.0314, -0.3485,  0.0479,
          0.0938,  0.0646,  0.6341,  0.0736,  0.3891, -0.0071,  0.0028, -0.1203,
          0.4763,  0.0085,  0.2327, -0.1183,  0.0727]])
```

### with 6 lengths hard-coded (no angles):

validation loss = about 3e-4

learned weights:

```
constrains.net.fc1.weight tensor([[-6.2344e-04, -5.1321e-01, -7.5215e-01, -5.2606e-01, -5.1544e-01,
         -3.2646e-01, -8.7965e-01, -4.3950e-01, -8.6010e-01, -4.3253e-01,
         -1.1046e+00, -4.5862e-01, -6.0943e-01]])
```