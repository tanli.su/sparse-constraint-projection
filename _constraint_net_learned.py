from torch import nn, Tensor
import torch

# constraint network with learned constraints for rigid_4

class MLP_Constraint(nn.Module):
    def __init__(self, num_particles, dimension, num_features):
        super(MLP_Constraint, self).__init__()
        self.num = num_particles
        self.dim = dimension
        self.net = nn.ModuleDict()
        self.net['fc1'] = nn.Linear(in_features = self.num*self.dim, out_features = 256, bias = False)
        self.net['acti1'] = nn.LeakyReLU()
        self.net['fc2'] = nn.Linear(in_features = 256, out_features = 10, bias = False)
        self.net['fc3'] = nn.Linear(in_features = 21, out_features = 1, bias = False)
        print(self.net)

    def forward(self, x):
        # learn 10 length/angle values
        learned = x.view([x.size()[0], x.size()[1]*x.size()[2]]) 
        learned = self.net['fc1'](learned)
        learned = self.net['acti1'](learned)
        learned = self.net['fc2'](learned)
        learned = torch.sum(learned, 0)
        # print("learned:", learned)

        B = x.size()[0]
        # m = x.size()[1]
        # n = x.size()[2]
        theta = torch.ones(B, 1).cuda() # constant term
        
        # get 6 lengths
        k = 0
        for i in range(4):
            for j in range(i+1, 4):
                length = ((x[:,i,0] - x[:,j,0]) ** 2 + (x[:,i,1] - x[:,j,1]) ** 2).view(B, 1).cuda() # distance squared between points i and j
                c = torch.abs(torch.sqrt(length) - learned[k])
                theta = torch.cat((theta, c, c*c), 1)
                k += 1
        
        # get 4 angles
        angles_list = [[2,0,1], [0,1,3], [1,3,2], [3,2,0]]
        for angles in angles_list:
            i = angles[0]; j = angles[1]; k = angles[2]
            v01 = x[:,i,:] - x[:,j,:]
            v02 = x[:,k,:] - x[:,j,:]
            d1 = torch.sqrt(v01[:,0] * v01[:,0] + v01[:,1] * v01[:,1])
            d2 = torch.sqrt(v02[:,0] * v02[:,0] + v02[:,1] * v02[:,1])
            dotprod = v01[:,0] * v02[:,0] + v01[:,1] * v02[:,1]
            angle = torch.acos(dotprod / (d1 * d2)).view(B, 1).cuda()
            c = torch.abs(angle - learned[k])
            theta = torch.cat((theta, c, c*c), 1)
            k += 1

        # print("theta.size: ", theta.size()) # B * 21
        out = self.net['fc3'](theta)
        # print("out.size: ", out.size())
        return out # B * 1 