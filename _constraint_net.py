from torch import nn, Tensor
import torch

# constraint network with hard-coded constraints
# for the lengths and angles in rigid_4

class MLP_Constraint(nn.Module):
    def __init__(self, num_particles, dimension, num_features):
        super(MLP_Constraint, self).__init__()
        self.net = nn.ModuleDict()
        self.net['fc1'] = nn.Linear(in_features = 21, out_features = 1, bias = False)
        print(self.net)

    def forward(self, x):
        B = x.size()[0]
        # m = x.size()[1]
        # n = x.size()[2]
        theta = torch.ones(B, 1).cuda() # constant term
        
        # get 6 lengths
        real_length = [0.6149, 0.6519, 0.9155, 0.6250, 0.4757, 0.4854]
        k = 0
        for i in range(4):
            for j in range(i+1, 4):
                length = ((x[:,i,0] - x[:,j,0]) ** 2 + (x[:,i,1] - x[:,j,1]) ** 2).view(B, 1).cuda() # distance squared between points i and j
                c = torch.abs(torch.sqrt(length) - real_length[k])
                theta = torch.cat((theta, c, c*c), 1)
                k += 1
        
        # get 4 angles
        real_angle = [1.0304, 1.9819, 1.4159, 1.8551]
        angles_list = [[2,0,1], [0,1,3], [1,3,2], [3,2,0]]
        k = 0
        for angles in angles_list:
            i = angles[0]; j = angles[1]; k = angles[2]
            v01 = x[:,i,:] - x[:,j,:]
            v02 = x[:,k,:] - x[:,j,:]
            d1 = torch.sqrt(v01[:,0] * v01[:,0] + v01[:,1] * v01[:,1])
            d2 = torch.sqrt(v02[:,0] * v02[:,0] + v02[:,1] * v02[:,1])
            dotprod = v01[:,0] * v02[:,0] + v01[:,1] * v02[:,1]
            angle = torch.acos(dotprod / (d1 * d2)).view(B, 1).cuda()
            c = torch.abs(angle - real_angle[k])
            theta = torch.cat((theta, c, c*c), 1)
            k += 1

        # print("theta.size: ", theta.size()) # B * 7
        out = self.net['fc1'](theta)
        # print("out.size: ", out.size())
        return out # B * 1 